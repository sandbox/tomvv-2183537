<?php
/**
 * @file
 * Google Universal Analytics administration functions
 */

/**
 * Form builder. Configure Google Universal Analytics.
 *
 * Implements hook_admin().
 */
function google_universal_analytics_admin($form, &$form_state) {
  $form = array();
  $form['#tree'] = TRUE;
  $dimensions = variable_get('google_universal_analytics_dimensions', array());

  // Check if Google Analytics module exists and is enabled.
  if (module_exists('googleanalytics')) {
    drupal_set_message(t('To properly use this module, please disable the Google Analytics module.'), 'error');
  }

  $form['google_universal_analytics_guacode'] = array(
    '#type' => 'textfield',
    '#title' => t('UA Code'),
    '#description' => t('Your unique UA Code, like UA-XXXX-Y'),
    '#default_value' => variable_get('google_universal_analytics_guacode', ''),
    '#required' => TRUE,
  );
  $form['google_universal_analytics_google_ranking_code'] = array(
    '#type' => 'fieldset',
    '#title' => t('Google Ranking'),
    '#prefix' => '<div id="ranking-code-fieldset-wrapper">',
    '#suffix' => '</div>',
    '#weight' => 1,
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => 'collapsed',
  );
  $form['google_universal_analytics_google_ranking_code']['enabled'] = array(
    '#type' => 'checkbox',
    '#title' => t('Add Google Ranking code'),
    '#default_value' => variable_get('google_universal_analytics_google_ranking_code_enabled', 1),
    '#required' => FALSE,
  );
  $form['google_universal_analytics_dimensions'] = array(
    '#type' => 'fieldset',
    '#title' => t('Dimensions'),
    '#prefix' => '<div id="dimensions-fieldset-wrapper">',
    '#suffix' => '</div>',
    '#weight' => 2,
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    '#collapsed' => 'collapsed',
  );
  if (!isset($form_state['num_dimensions'])) {
    $form_state['num_dimensions'] = count($dimensions) - 1;
  }
  for ($i = 0; $i <= $form_state['num_dimensions']; $i++) {

    $title = (isset($dimensions[$i]['google_universal_analytics_description']) ? $dimensions[$i]['google_universal_analytics_description'] . ' [dimension' . $dimensions[$i]['google_universal_analytics_dimensionid'] . ']' : t('New dimension'));
    $collapsed = (isset($form_state['newdimension']) ? FALSE : TRUE);

    $form['google_universal_analytics_dimensions'][$i] = array(
      '#type' => 'fieldset',
      '#title' => $title,
      '#prefix' => '<div id="dimensions-fieldset-wrapper">',
      '#suffix' => '</div>',
      '#weight' => 1,
      '#tree' => TRUE,
      '#collapsible' => TRUE,
      '#collapsed' => $collapsed,
    );
    $form['google_universal_analytics_dimensions'][$i]['google_universal_analytics_description'] = array(
      '#type' => 'textfield',
      '#title' => t('Description for this dimension (for administrational purpose only)'),
      '#default_value' => $dimensions[$i]['google_universal_analytics_description'],
      '#required' => TRUE,
    );
    $form['google_universal_analytics_dimensions'][$i]['google_universal_analytics_pathlist'] = array(
      '#type' => 'textarea',
      '#title' => t('List of urls to match'),
      '#default_value' => $dimensions[$i]['google_universal_analytics_pathlist'],
      '#required' => FALSE,
    );
    $form['google_universal_analytics_dimensions'][$i]['google_universal_analytics_dimensionid'] = array(
      '#type' => 'select',
      '#title' => t('Dimension id'),
      '#options' => array(
        1 => 1,
        2 => 2,
      ),
      '#default_value' => $dimensions[$i]['google_universal_analytics_dimensionid'],
      '#required' => TRUE,
    );
    $form['google_universal_analytics_dimensions'][$i]['google_universal_analytics_dimensionname'] = array(
      '#type' => 'textfield',
      '#title' => t('Dimension name'),
      '#default_value' => $dimensions[$i]['google_universal_analytics_dimensionname'],
      '#required' => TRUE,
    );
    if ($form_state['num_dimensions'] > 0 && $i < $form_state['num_dimensions']) {
      $form['google_universal_analytics_dimensions'][$i]['remove_dimension'] = array(
        '#type' => 'submit',
        '#name' => $i,
        '#value' => t('Remove this dimension'),
        '#submit' => array('google_universal_analytics_dimension_add_more_remove_specific'),
        '#ajax' => array(
          'callback' => 'google_universal_analytics_dimension_add_more_callback',
          'wrapper' => 'dimensions-fieldset-wrapper',
        ),
      );
    }
  }

  $form['google_universal_analytics_dimensions']['add_dimension'] = array(
    '#type' => 'submit',
    '#value' => t('Add dimension'),
    '#submit' => array('google_universal_analytics_dimension_add_more_add_one'),
    '#ajax' => array(
      'callback' => 'google_universal_analytics_dimension_add_more_callback',
      'wrapper' => 'dimensions-fieldset-wrapper',
    ),
  );

  if ($form_state['num_dimensions'] > 0) {
    $form['google_universal_analytics_dimensions']['remove_dimension'] = array(
      '#type' => 'submit',
      '#value' => t('Remove last dimension'),
      '#submit' => array('google_universal_analytics_dimension_add_more_remove_one'),
      '#ajax' => array(
        'callback' => 'google_universal_analytics_dimension_add_more_callback',
        'wrapper' => 'dimensions-fieldset-wrapper',
      ),
    );
  }
  return system_settings_form($form);
}

/**
 * Helper function for the addition of dimensions.
 */
function google_universal_analytics_dimension_add_more_add_one($form, &$form_state) {
  $form_state['num_dimensions']++;
  $form_state['newdimension'] = TRUE;
  $form_state['rebuild'] = TRUE;
}

/**
 * Helper function to return the dimensions.
 */
function google_universal_analytics_dimension_add_more_callback($form, $form_state) {
  return $form['google_universal_analytics_dimensions'];
}

/**
 * Helper function for the removal of the last dimension.
 */
function google_universal_analytics_dimension_add_more_remove_one($form, &$form_state) {
  if ($form_state['num_dimensions'] > 0) {
    $form_state['num_dimensions']--;
  }
  $form_state['rebuild'] = TRUE;
}

/**
 * Helper function for the removal of a specific dimension.
 */
function google_universal_analytics_dimension_add_more_remove_specific($form, &$form_state) {
  $to_be_removed = $form_state['triggering_element']['#name'];
  $dimensions = variable_get('google_universal_analytics_dimensions');
  unset($dimensions[$to_be_removed]);
  variable_del('google_universal_analytics_dimensions');
  variable_set('google_universal_analytics_dimensions', array_values($dimensions));

  if ($form_state['num_dimensions'] > 0) {
    $form_state['num_dimensions']--;
  }
  $form_state['rebuild'] = TRUE;
}
