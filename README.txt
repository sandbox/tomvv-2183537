
Module: Google Universal Analytics
Author: Tom van Vliet <https://drupal.org/user/2748021>


Description
===========
description = This module implements Google Universal Analytics features.

Requirements
============
* Google Analytics user account

Installation
============
* Copy the 'google_universal_analytics' module directory in to your Drupal
sites/all/modules directory as usual.

Usage
=====
* In the settings page, the Google Analytics account information can be added.
* It is possible to add dimensions to the tracking code.
